---
layout: markdown_page
title: "Category Direction - Runner Core"
description: "GitLab Runner is the multi-platform execution agent that works with GitLab CI to execute the jobs in your pipelines. View more information here!"
canonical_path: "/direction/verify/runner/"
---

- TOC
  {:toc}

## Vision

Our vision for GitLab Runner is to offer DevOps teams a CI/CD code execution agent compatible with the market-leading computing architectures and platforms, is highly performant and scalable, and meets the most stringent security and compliance requirements. The performance and scalability of the CI/CD build agents are essential enablers of the OPS section's goal of establishing GitLab CI/CD as the most efficient, speedy, and reliable CI/CD pipeline solution in the market.

## Who we are focusing on?

Check out our [Ops Section Direction "Who's is it for?"](/direction/ops/#who-is-it-for) for an in depth look at the our target personas across Ops. For Runner, our "What's Next & Why" are targeting the following personas, as ranked by priority for support:

1. [Priyanka - Platform Engineer](/handbook/marketing/strategic-marketing/roles-personas/#priyanka-platform-engineer)
2. [Devon - DevOps Engineer](/handbook/marketing/strategic-marketing/roles-personas/#devon-devops-engineer)
3. [Sasha - Software Developer](/handbook/marketing/strategic-marketing/roles-personas/#sasha-software-developer)
4. [Delaney - Development Team Lead](h/handbook/marketing/strategic-marketing/roles-personas/#delaney-development-team-lead)

## Supported Compute Architectures & Platforms

### OS + Architecture

| Operating Systems | Compute Architectures | 
|----------|----------------|
|Linux|x86_64, ARM32, ARM64, ppc64le|
|macOS|x86_64, M1|
|Windows|x86_64|
|Linux on Z (IBM)|s390x|

### Containers & Container Orchestration

|||
|----------|----------------|
|Containers|Docker|
|Container Orchestration|Kubernetes, Red Hat Open Shift, AWS Fargate, AWS EKS, GCP ECS

## Strategic Priorities - What's next & Why?

The table below represents the current strategic priorities for runner core. This list will change with each monthly revision of this direction page.

|Theme-Category|Item|Why?|Target delivery QTR|
|----------|----------------|----------------|----------------|
|Security and Compliance|[GitLab Runner FIPS Compliance](https://gitlab.com/groups/gitlab-org/-/epics/6481)|Creating a FIPS compliant GitLab Runner distribution is an enabler for our strategic objectives.|FY22 Q4|
|Platform Enablement|[Docker Machine Autoscaling Strategy](https://gitlab.com/gitlab-org/gitlab/-/issues/341856)|Finalizing our technical approach to providing self-managed users an easy to implement and maintain autoscaling runner solution is critical as users and customers rely on the Docker Machine autoscaling solution for production CI workload executions.|FY23 Q1|
|Security and Compliance|[GitLab Runner Security Enhancements](https://gitlab.com/groups/gitlab-org/-/epics/4791)|A GitLab Runner can be described as a worker process that executes the CI/CD pipeline jobs you define for your project. A runner has access to the source code in your project repository, so you must follow best practices for securely running your CI/CD jobs. One of our goals in FY23 is to enhance the security architecture of the runner token mechanism to reduce security risks further and simplify compliance management.|FY23 Q2|
|Secure Software Supply Chain|[Artifact Metadata Datastore integration](https://gitlab.com/groups/gitlab-org/-/epics/6207)|"The software of complex systems is often built from many discrete software modules that perform distinct functions. Modern software can be rapidly or even automatically assembled. In this respect, software development increasingly resembles manufacturing processes." Secure software supply chain management, and the related Software Bill of Materials (SBOM), refers to ensuring the security and provenance of everything that goes into the software you build and ultimately deploy to a production environment.|FY23 Q2|
|Security and Compliance|[GitLab CI - Namespaced or Protected GitLab Runner Tags](https://gitlab.com/gitlab-org/gitlab/-/issues/329344)|A critical use case for organizations with stringent security and compliance requirements is the ability to define that highly sensitive CI jobs are executed on authorized remote code execution targets (runners). Implementing a namespaced or protected tagging feature for runners means running specific CI jobs in a secure and authorized environment.|FY23 Q3|
|Security and Compliance|[GitLab Runner Architectural Evolution](https://gitlab.com/groups/gitlab-org/-/epics/5422)|The current GitLab Runner model in which a runner is strictly coupled with a type has worked well. However - there are several use cases, especially around addressing regulatory and compliance requirements that will require migrating from the concept of runner types to a runner ownership model. Once implemented, such a model will enable customers to quickly implement configurations such as limiting access to a Runner to specified users or groups.|FY23 Q4|

Refer to the [Runner Core roadmap board](https://gitlab.com/gitlab-org/gitlab-runner/-/issues?scope=all&state=opened&label_name[]=direction) for a more in-depth view of the planned roadmap.

## Ongoing Maintenance

In conjunction with the development work required to deliver the strategic priorities listed above, in each milestone, the Runner Core team will devote up to 40% of available developer capacity across the categories listed below.

- Bugs: The primary focus through FY23Q2 is the burndown of the [aged P2 bug backlog](https://gitlab.com/gitlab-org/gitlab-runner/-/issues?sort=created_asc&state=opened&label_name[]=type::bug&label_name[]=priority::2&label_name[]=missed-SLO).
- Go or other dependency updates.
- Kubernetes Executor: bugs & new feature development.
- Linux OS maintenance support.
- Windows OS mainteance support.
- macOS maintenance support.
- GitLab Runner Red Hat OpenShift Operator maintenance & new features.
- GitLab Runner `custom executor` driver maintenance & new features.
- GitLab Runner AWS Fargate driver maintenance & new features.
- Other features or enhancements to the core runner codebase.
- Community contributions.

## Maturity Plan

- Runner core is at the "Lovable" maturity level (see our [definitions of maturity levels](/direction/maturity/)).
- As detailed in this [epic](https://gitlab.com/groups/gitlab-org/-/epics/6090), we plan to review the maturity scorecard for runner core and complete new category maturity scorecards for the other product development categories, runner cloud, and fleet management.

## Competitive Landscape

When you run a continuous integration pipeline job or workflow, the code in that pipeline must execute on some computing platform to complete your software's building, testing, and deployment. Terms used to describe the software that handles the pipeline code execution include worker, agent, or runner.

So while the basic functionality of pipeline code execution is table stakes in the industry, the ability to efficiently build software on multiple compute platforms with low operational maintenance overhead is critical to the value proposition for self-managed GitLab.

### GitLab Runner Value Proposition

For customers who need to run CI/CD workloads on environments that they manage (self-managed), GitLab runner includes a wide array of features and capabilities positioned competitively in the marketplace.

1. GitLab Runner is open-source. Our community members and customers have full access to the GitLab Runner source code and can contribute features, bug fixes directly to the code base.
1. GitLab Runner supports multiple executors. One of the most useful executor types is the Docker executor, which enables users to execute CI jobs inside a container, resulting in less maintenance for the CI/CD build environment.
1. GitLab Runner includes CI job execution autoscaling out of the box. Autoscaling is available for public cloud virtual machines or on Kubernetes clusters.
1. GitLab Runner supports several computing architectures. Customers who need to self-manage runners on platforms such as IBM Z mainframes to take advantage of GitLab and modern Value Stream Delivery Platforms can. We aim to meet and support customers on the platforms that they use in their environment.
1. A significant competitive differentiator is the availability of the GitLab Runner [custom executor](https://gitlab.com/gitlab-org/ci-cd/custom-executor-drivers). This open-source solution enables the community to add an executor to the GitLab Runner to execute CI jobs on a computing platform or infrastructure that's not currently supported. With this very powerful yet simple to implement driver, users can configure the GitLab Runner to use an executable to provision, run and clean up

### Competitive Matrix

|Solution|CI/CD Agent naming convention/brand|Self-Managed Option Availablity|Notes|
|----------|----------------|----------------|----------------|
|GitHub Actions|Runners|Available|GitHub released self-hosted runners in late 2019. Since then, GitHub has continued to invest in features and capabilities. As GitHub continues to target market segments requiring a self-managed platform, we also notice similar themes. For example, a feature related to the ease of use and enterprise management theme, [an improved Runner management experience](https://github.blog/changelog/2021-09-20-github-actions-experience-refresh-for-the-management-of-self-hosted-runners/) released in September 2021. For security and compliance, the [Limit self-hosted Runners to specific workflows](https://github.com/github/roadmap/issues/220) feature is planned in Q1 2022.| 
|Jenkins|Agent|Available|A Jenkins agent is an executable residing on a node, whether virtual, bare-metal or a container that the Jenkins controller tasks to run a job. While installing the Jenkins agent on a target platform does require Java, the agent capability enables distributed builds in Jenkins and is flexible from a deployment standpoint. The Jenkins agent architecture is scalable; however, there will be ongoing maintenance overhead for organizations that self-manage large-scale Jenkins installations.|
|Harness.io|Harness Delegate|Available|Harness currently provides the following types of Delegate: Kubernetes, Shell Script, AWS ECS, Helm, Docker. Though the Delegates perform a similar essential function as GitLab Runner, i.e., executes tasks provided by the Harness Manager, the Delegates' primary purpose is to deploy software to the target platform. In this regard, the value proposition of the [GitLab Agent for Kubernetes](https://docs.gitlab.com/ee/user/clusters/agent/) is a critical consideration when evaluating capabilities in GitLab for developer frictionless cloud-native deployment.|
|Codefresh|Codefresh Runner|Available*|The Codefresh Runner, which handles getting tasks from the Codefresh SaaS platform and executing them, is available only for Kubernetes.|
|CircleCI|CircleCI Runner|Available|The CircleCi self-hosted runner, released in November 2020, is supported on Linux, Windows, macOS, and Kubernetes but is only available to customers on CircleCi's Scale Plan. In the near term, CirlceCI is adding support for additional platforms. Extending platform support is an expected and necessary by-product of targeting customers who cannot run CI/CD workloads on a SaaS solution.|

### Threat of new entrants

The pace of change and innovation in DevOps is high. New entrants will likely challenge current paradigms and disrupt the market.  An example of that is [onedev](https://github.com/theonedev/onedev), an open-source project that relies solely on Kubernetes to execute CI jobs with Linux and Windows containers support. The long-term potential here is clear. Kubernetes continues to be the leading container orchestration platform. Assuming that continues and organizations develop a deep bench of expertise to manage Kubernetes at scale, then we can make the following hypothesis. Having a CI/CD runner solution that is easy to install, maintain and operate on Kubernetes, coupled with predictive DevOps capabilities, will be critical to long-term market success.

So, as we head into FY23 and beyond, we will continue to focus on adding key features to Runner Core to maintain our pace of innovation and competitive position

## Give Feedback

The near features highlighted here represent just a subset of the features and capabilities that have been requested by the community and customers. If you have questions about a specific runner feature request or have a requirement that's not yet in our backlog, you can provide feedback or open an issue in the GitLab Runner [repository](https://gitlab.com/gitlab-org/gitlab-runner/-/issues).

## Revision Date

This direction page was revised on: 2022-01-09
