---
layout: markdown_page
title: "Direction - Remote Development"
description: This is the direction page for Remote Development in GitLab.
canonical_path: "/direction/create/remote_development/"
---

- TOC
{:toc}

## Remote Development

| Section | Stage | Maturity | Last Reviewed |
| --- | --- | --- | --- |
| [Dev](/direction/dev/) | [Create](https://about.gitlab.com/stages-devops-lifecycle/create/) | [Planned](/direction/maturity/) | 2022-01-21 |

### Introduction and how you can help

This is the direction page for Remote Development in GitLab. It is not officially a part of any DevOps stage nor is it an official marketing category. Eric Schurter ([@ericschurter](https://gitlab.com/ericschurter), [eschurter@gitlab.com](mailto:eschurter@gitlab.com)) Senior Product Manager for the [Editor group](/direction/create/editor/), maintains this page and the vision, as it relates closely to the [Web IDE](/direction/create/editor/web_ide/) and related features.

Like most other category direction pages, this page will be continuously updated based on market dynamics, new data points, and customer conversations. Sharing feedback directly on the [category direction epic](https://gitlab.com/groups/gitlab-org/-/epics/7419) or specific issues contained within is the best way to contribute to our strategy and vision. 

### Overview

One of the first barriers to overcome before contributing to a project is configuring your local development environment. The time-consuming task of managing related dependencies and troubleshooting incompatible versions can be discouraging, especially for someone working who contributes infrequently or works on multiple projects. In more advanced environments, a development team may even have a standard set of tools, extensions, and configuration files, adding to the delicate nature of the development environment.

The GitLab Web IDE allows for anyone to contribute to a project right from their web browser. This means making a simple fix to an existing project or responding to feedback in a merge request can be completed quickly and effectively. However, without code linting, live preview, or the ability to run tests, many developers are unable to complete meaningful work in the Web IDE. The Web Terminal and Live Preview features offer some additional functionality for a narrow set of use cases, and code changes in the Web IDE can trigger a pipeline, but that doesn't provide the real-time feedback necessary for efficient development. 

Developers want to spend less time managing their development environment and more time contributing high-quality code. And at GitLab, we want _everyone_ to contribute. Eliminating the responsibility of configuring and maintaining a local development environment frees up valuable development time and streamlines onboarding of new developers joining the team. For developers working on larger teams, or those contributing to multiple open source projects, the cost of switching contexts can be so high that it can discourage collaboration. With an on-demand, cloud-based development environment code reviews are less of a disruption because developers can move more quickly from project to project and sensitive data is securely stored in the cloud rather than distributed across numerous local devices.

<!-- ### Performance indicators

#### Primary metric

#### Secondary metrics -->

### Target audience

Remote Development, as the name suggests, directly targets software developers and those who manage or support teams of developers. However, a mature remote development offering lowers the technical barrier to collaboration and enables non-developer personas to effectively and efficiently contribute. As a result, we are able to target all the [user personas](/handbook/marketing/strategic-marketing/roles-personas/#user-personas) we describe in our handbook, with a particular focus on:

- **[Sasha (Software Developer):](/handbook/marketing/strategic-marketing/roles-personas/#sasha-software-developer)** targets full time contributors to all types of projects (commercial, OSS, data science, etc.). These users expect and need a high level of reliability and speed in their interactions with both project files and Git.

- **[Delaney (Development Team Lead):](/handbook/marketing/strategic-marketing/roles-personas/#delaney-development-team-lead)** targets users who often times have elevated roles which allow for the management of project settings, such as access control, security, commit strategies, and mirroring.

- **[Devon (DevOps Engineer):](/handbook/marketing/strategic-marketing/roles-personas/#devon-devops-engineer)** targets engineers tasked with supporting and enabling software teams. Their tasks often revolve around platform creation and maintenance, where [GitOps](/topics/gitops/)] workflows are crucial.

<!-- ### Challenges to address -->

<!-- - What needs, goals, or jobs to be done do the users have?
- How do users address these challenges today? What products or work-arounds are utilized?

Provide links to UX Research issues, which validate these problems exist. -->

<!-- - conflicting dependencies
- inefficient troubleshooting -->

<!-- ### Where we are headed -->

<!--
Describe the future state for your category.
- What problems are we intending to solve?
- How will GitLab uniquely address them?
- What is the resulting benefits and value to users and their organizations?

Use narrative techniques to paint a picture of how the lives of your users will benefit from using this category once your strategy is at least minimally realized.
-->

<!-- - ideal user experience, user journey(s)
- principles 
- future state -->

<!-- #### What's Next & Why

#### What is not planned right now -->

#### Maturity plan 

<!--
It's important your users know where you're headed next.
The maturity plan section captures this by showing what's required to achieve the next level.
-->

This category is currently `Planned` with no specific date for moving to `Minimal`. 

As part of the [Web IDE](/direction/create/editor/web_ide/) strategy, we are investigating whether to replace the existing Web IDE with a [web-based instance of VS Code](https://gitlab.com/groups/gitlab-org/-/epics/7143). This is the first step toward a mature remote development platform. One potential path we may take would be: 

1. Replace the Web IDE with VS Code, improving the user experience and performance while introducing extensibility and customization
2. Improve the development experience for JavaScript-based projects by maturing the Live Preview feature inside the new Web IDE
3. Introduce cloud-based development environments that can be launched from the Web IDE
4. Allow secure access to remote development environments from local IDEs

### Competitive Landscape

The three most relevant competitors in this area are [GitHub Codespaces](https://github.com/features/codespaces), [Gitpod](https://www.gitpod.io/), and [Coder](https://coder.com/). Each one offers a slightly different take on remote development environments but aim to solve very similar problems. 

These companies and projects are also related to the remote development space:

- [Theia](https://theia-ide.org/)
- [Eclipse Che](https://www.eclipse.org/che/)
- [CodeSandbox](https://codesandbox.io/)
- [Cloud9 IDE](https://aws.amazon.com/cloud9/)
- [Codeanywhere](https://codeanywhere.com/)
- [JetBrains](https://www.jetbrains.com/fleet/)


<!-- ### Analyst Landscape -->

<!-- What analysts and/or thought leaders in the space talking about, what are one or two issues
that will help us stay relevant from their perspective.-->

### Related issues

<!-- #### Top user issues

#### Top dogfooding issues

#### Top vision items -->

- [IDE / editor with a local editor via mirroring / sync](https://gitlab.com/gitlab-org/gitlab/-/issues/16069)
- [Remote Development Environments](https://gitlab.com/groups/gitlab-org/-/epics/3230)
- [Server Runtime](https://gitlab.com/gitlab-org/gitlab/-/issues/329602)
- [Server side evaluation in the Web IDE](https://gitlab.com/groups/gitlab-org/-/epics/167)
- [Web IDE Replacement w/ Theia](https://gitlab.com/groups/gitlab-org/-/epics/1619)

#### Opportunity reviews

- [Previous opportunity canvas](https://docs.google.com/document/d/1t1j98Wl1erG9b8cUT77yDsNUEPvCOMOp0ktbOkYZfWc/edit#heading=h.4mt5fmtn0ax4)
- [Investigation into replacing Web IDE with Theia](https://gitlab.com/groups/gitlab-org/-/epics/1619)