---
layout: handbook-page-toc
title: "GitLab Product Training for Customer Success"
description: "As a Solutions Architect, Technical Account Manager, or Professional Services Engineer, it is important to be continuously learning more about our product and related industry topics. This handbook page provides an dashboard of aggregated resources that we encourage you to use to get up to speed."
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Overview of GitLab Product Training 

As a Solutions Architect, Technical Account Manager, or Professional Services Engineer, it is important to be continuously learning more about our product and related industry topics. This handbook page provides an dashboard of aggregated resources that we encourage you to use to get up to speed. Although we aggregated and/or developed these resources for Customer Success team members, they are generic enough that all team members and partners can benefit from the education and enablement resources that we have published here.


## Overview of GitLab Feature and Use Cases

The GitLab product is organized based on the DevOps stages.

### DevOps Stages

If you're new to GitLab and the concept of "stages" are new to you, simply think of it as a category of related features that a customer finds value in with a **technical** narrative. The stages are organized in an infinite loop diagram to show the software development lifecycle from planning to development to release, with additional value for deployed applications with security and monitoring features. You can see a high-level overview of each stage on the [marketing page](https://about.gitlab.com/stages-devops-lifecycle/). Be sure to click the `Learn More` link for each stage to see more about the features of that stage.

One of the most helpful resources for understanding what GitLab does, the features in each stage, and how well the feature works is our [Category maturity](https://about.gitlab.com/direction/maturity/) chart. This may also be informally referred to as our "lovable feature chart".

If you understand what the stages are and want to dive deeper, you can look at the [product categories handbook page](https://about.gitlab.com/handbook/product/categories/) for a deep-dive on each of the stages, specifically the [hierarchy](https://about.gitlab.com/handbook/product/categories/#hierarchy) and the [DevOps Stages](https://about.gitlab.com/handbook/product/categories/#devops-stages).

For the purposes of education and enablement, we focus on the DevOps stages that the customer sees. There are additional stages that help GitLab as a business succeed, such as the [Growth stage](https://about.gitlab.com/handbook/product/categories/#growth-stage), [Fulfillment stage](https://about.gitlab.com/handbook/product/categories/#fulfillment-stage), [Enablement stage](https://about.gitlab.com/handbook/product/categories/#enablement-stage), and [Single Engineer Groups stage](https://about.gitlab.com/handbook/product/categories/#single-engineer-groups-section). Although these are not necessarily DevOps stages, the Engineering and Product departments use the stage terminology for consistency. **You do not need to spend time learning about these stages and can search for this information later if the need arises.**

## GitLab Product Topics

To align our education and enablement with our product, each of the topics below are grouped based on how GitLab product management and engineering defines it.

### DevOps Sections
***
##### Foundations Level
- [Dev Section Direction and Roadmap](https://about.gitlab.com/direction/dev/)
- [Ops Section Direction and Roadmap](https://about.gitlab.com/direction/ops/)
- [Security Section Direction and Roadmap](https://about.gitlab.com/direction/security/)

### Manage Stage
***
##### Foundations Level
- [Manage Stage Overview](https://about.gitlab.com/stages-devops-lifecycle/manage/)
- [Manage Stage Direction and Roadmap](https://about.gitlab.com/direction/dev/#manage)
##### Intermediate Level
- [Value Stream Management Feature Docs](https://about.gitlab.com/solutions/value-stream-management/)
- [Audit Events Feature Docs](https://docs.gitlab.com/ee/administration/audit_events.html)
- [DevOps Reports Feature Docs](https://docs.gitlab.com/ee/user/admin_area/analytics/dev_ops_report.html)
- [Code Analytics Feature Docs](https://docs.gitlab.com/ee/user/analytics/code_review_analytics.html)
- [Compliance Management Feature Docs](https://docs.gitlab.com/ee/administration/compliance.html)
- [Audit Reports Feature Docs](https://docs.gitlab.com/ee/administration/audit_reports.html)

### Plan Stage
***
##### Foundations Level
- [Plan Stage Overview](https://about.gitlab.com/stages-devops-lifecycle/plan/)
- [Plan Stage Direction and Roadmap](https://about.gitlab.com/direction/dev/#plan)
##### Intermediate Level
- [Issue Tracking Feature Docs](https://docs.gitlab.com/ee/user/project/issues/)
- [Time Tracking Feature Docs](https://about.gitlab.com/solutions/time-tracking/)
- [Boards Feature Docs](https://about.gitlab.com/stages-devops-lifecycle/issueboard/)
- [Epics Feature Docs](https://about.gitlab.com/product/epics/)
- [Roadmaps Feature Docs](https://about.gitlab.com/stages-devops-lifecycle/roadmaps/)
- [Service Desk Feature Docs](https://about.gitlab.com/stages-devops-lifecycle/service-desk/)
- [Requirements Management Feature Docs](https://docs.gitlab.com/ee/user/project/requirements/)
- [Quality Management Feature Docs](https://docs.gitlab.com/ee/ci/test_cases/index.html)
- [Design Management Feature Docs](https://docs.gitlab.com/ee/user/project/issues/design_management.html)

### Create Stage
***
##### Foundations Level
- [Create Stage Overview](https://about.gitlab.com/stages-devops-lifecycle/create/)
- [Create Stage Direction and Roadmap](https://about.gitlab.com/direction/dev/#create)
##### Intermediate Level
- [Source Code Management Feature Overview](https://about.gitlab.com/stages-devops-lifecycle/source-code-management/)
- [Source Code Management Direction and Roadmap](https://about.gitlab.com/direction/create/source_code_management/)
- [Code Review Feature Overview with 30+ Features](https://about.gitlab.com/stages-devops-lifecycle/code-review/)
  - [JIRA Integration Solution Overview](https://about.gitlab.com/solutions/jira/)
  - [JIRA Issues Integration Feature Docs](https://docs.gitlab.com/ee/integration/jira/)
  - [JIRA Development Panel Integration Feature Docs](https://docs.gitlab.com/ee/integration/jira/dvcs.html)
  - [Multiple approvers in code review Feature Docs](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/rules.html)
  - [Approval rules for code review Feature Docs](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/rules.html)
  - [Merge request dependencies Feature Docs](https://docs.gitlab.com/ee/user/project/merge_requests/merge_request_dependencies.html)
  - [Code Owners Feature Docs](https://docs.gitlab.com/ee/user/project/code_owners.html)
- [Wiki Feature Docs](https://docs.gitlab.com/ee/user/project/wiki/)
- [Static Site Editor Feature Docs](https://docs.gitlab.com/ee/user/project/static_site_editor/)
- [Web IDE Feature Docs](https://docs.gitlab.com/ee/user/project/web_ide/index.html)
- [Live Preview Feature Docs](https://docs.gitlab.com/ee/user/project/web_ide/index.html#live-preview)
- [Snippets Feature Docs](https://docs.gitlab.com/ee/user/snippets.html)
- [Gitaly Source Code Project](https://gitlab.com/gitlab-org/gitaly)

### Verify Stage
***
##### Foundations Level
- [Verify Stage Overview](https://about.gitlab.com/stages-devops-lifecycle/verify/)
- [Verify Stage Direction and Roadmap](https://about.gitlab.com/direction/ops/#verify)
- [CI YouTube Playlist](https://youtube.com/playlist?list=PL05JrBw4t0Ko-mJZLo2uF3aQQuBfBaSKB)
##### Intermediate Level
- [Continuous Integration (CI) Feature Overview](https://about.gitlab.com/stages-devops-lifecycle/continuous-integration/)
  - [CI/CD Feature Docs](https://docs.gitlab.com/ee/ci/)
  - [CI/CD Concepts Docs](https://docs.gitlab.com/ee/ci/introduction/)
  - [CI/CD Pipeline Docs](https://docs.gitlab.com/ee/ci/pipelines/)
  - [CI/CD Variables Docs](https://docs.gitlab.com/ee/ci/variables/README.html)
  - [CI/CD Environments and Deployments Docs](https://docs.gitlab.com/ee/ci/environments/)
  - [CI/CD with Runners Docs](https://docs.gitlab.com/ee/ci/runners/README.html)
  - [Auto DevOps Docs](https://docs.gitlab.com/ee/topics/autodevops/)
  - [CI/CD configuration with `.gitlab-ci.yml` Docs](https://docs.gitlab.com/ee/ci/yaml/gitlab_ci_yaml.html)
  - [CI/CD authoring YAML reference for `.gitlab-ci.yml` Docs](https://docs.gitlab.com/ee/ci/yaml/README.html)
  - [CI/CD Implementation Examples Docs](https://docs.gitlab.com/ee/ci/examples/README.html)
  - [CI/CD Troubleshooting Docs](https://docs.gitlab.com/ee/ci/troubleshooting.html)
  - [Migrate from CircleCI Docs](https://docs.gitlab.com/ee/ci/migration/circleci.html)
  - [Migrate from Jenkins Docs](https://docs.gitlab.com/ee/ci/migration/jenkins.html)
- [Code Quality Feature Docs](https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html)
- [Code Testing and Coverage Feature Docs](https://docs.gitlab.com/ee/ci/unit_test_reports.html)
- [Load Testing Feature Docs](https://docs.gitlab.com/ee/user/project/merge_requests/load_performance_testing.html)
- [Browser Performance Testing Feature Docs](https://docs.gitlab.com/ee/user/project/merge_requests/browser_performance_testing.html)
- [Usability Testing Feature Docs](https://docs.gitlab.com/ee/ci/review_apps/#visual-reviews-starter)
- [Accessibility Testing Feature Docs](https://docs.gitlab.com/ee/user/project/merge_requests/accessibility_testing.html)
- [Merge Trains Feature Docs](https://docs.gitlab.com/ee/ci/pipelines/merge_trains.html)

### Package Stage
***
##### Foundations Level
- [Package Stage Overview](https://about.gitlab.com/stages-devops-lifecycle/package/)
- [Package Stage Direction and Roadmap](https://about.gitlab.com/direction/package/)
##### Intermediate Level
- [Package Registry Feature Docs](https://docs.gitlab.com/ee/user/packages/package_registry/)
- [Container Registry Feature Docs](https://docs.gitlab.com/ee/user/packages/container_registry/)
- [Helm Chart Registry Feature Docs](https://docs.gitlab.com/ee/user/packages/container_registry/#use-the-container-registry-to-store-helm-charts)
- [Dependency Proxy Feature Docs](https://docs.gitlab.com/ee/user/packages/dependency_proxy/)
- [Dependency Firewall Direction and Roadmap](https://about.gitlab.com/direction/package/#dependency-firewall)
- [Release Evidence Feature Docs](https://docs.gitlab.com/ee/user/project/releases/#release-evidence)
- [Git LFS - Feature Docs](https://docs.gitlab.com/ee/topics/git/lfs/index.html)

### Release Stage
***
##### Foundations Level
- [Release Stage Overview](https://about.gitlab.com/stages-devops-lifecycle/release/)
- [Release Stage Direction and Roadmap](https://about.gitlab.com/direction/deployment)
- [CI/CD eBook](https://about.gitlab.com/resources/ebook-single-app-cicd/)
- [CI/CD YouTube Demo Overview](https://about.gitlab.com/blog/2017/03/13/ci-cd-demo/)
##### Intermeiate Level
- [Continuous Delivery Feature Overview](https://about.gitlab.com/stages-devops-lifecycle/continuous-integration/)
  - [See verify stage for full list of CI/CD resources](#verify-stage)
- [Pages Feature Overview](https://about.gitlab.com/stages-devops-lifecycle/pages/)
  - [Pages Feature Direction and Roadmap](https://about.gitlab.com/direction/release/pages/)
  - [Pages Feature Docs](https://docs.gitlab.com/ee/user/project/pages/)
- [Review Apps Feature Overview](https://about.gitlab.com/stages-devops-lifecycle/review-apps/)
  - [YouTube Webcast Feature Overview](https://www.youtube.com/watch?v=CteZol_7pxo&feature=youtu.be)
  - [Review Apps Direction and Roadmap](https://gitlab.com/groups/gitlab-org/-/epics/495)
  - [Review Apps Feature Docs](https://docs.gitlab.com/ee/ci/review_apps)
- [Advanced Deployments Feature Docs](https://docs.gitlab.com/ee/topics/autodevops/index.html#incremental-rollout-to-production-premium)
- [Feature Flags Feature Docs](https://docs.gitlab.com/ee/operations/feature_flags.html)
- [Release Orchestration Feature Docs](https://docs.gitlab.com/ee/user/project/releases/)

### Configure Stage
***
##### Foundations Level
- [Configure Stage Overview](https://about.gitlab.com/stages-devops-lifecycle/configure/)
- [Configure Stage Direction and Roadmap](https://about.gitlab.com/direction/configure/)
##### Intermediate Level
- [Auto DevOps Feature Overview](https://about.gitlab.com/stages-devops-lifecycle/auto-devops/)
- [Auto DevOps Feature Docs](https://docs.gitlab.com/ee/topics/autodevops/)
- [Kubernetes Management Feature Overview](https://about.gitlab.com/solutions/kubernetes/)
- [Secrets Management CI Variable Docs](https://docs.gitlab.com/ee/ci/variables/)
- [ChatOps Feature Docs](https://docs.gitlab.com/ee/ci/chatops/)
- [Serverless Feature Overview](https://about.gitlab.com/topics/serverless/)
- [Infrastructure-as-Code Feature Docs](https://docs.gitlab.com/ee/user/infrastructure/)
- [Cluster Cost Management Feature Docs](https://docs.gitlab.com/ee/user/clusters/cost_management.html)

### Monitor Stage
***
##### Foundations Level
- [Monitor Stage Overview](https://about.gitlab.com/stages-devops-lifecycle/monitor/)
- [Monitor Stage Direction and Roadmap](https://about.gitlab.com/direction/monitor/)
##### Intermediate Level
- [Runbooks Feature Docs](https://docs.gitlab.com/ee/user/project/clusters/runbooks/)
- [Metrics Feature Docs](https://docs.gitlab.com/ee/operations/metrics/)
- [Incident Management Feature Docs](https://docs.gitlab.com/ee/operations/incident_management/)
- [Logging Feature Docs](https://docs.gitlab.com/ee/user/project/clusters/kubernetes_pod_logs.html#kubernetes-pod-logs)
- [Tracing Feature Docs](https://docs.gitlab.com/ee/operations/tracing.html)
- [Error Tracking Feature Docs](https://docs.gitlab.com/ee/operations/error_tracking.html)
- [Synthetic Monitoring Direction and Roadmap](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8338)
- [Product Analytics Feature Docs](https://docs.gitlab.com/ee/operations/product_analytics.html)

### Secure Stage
***
##### Foundations Level
- [Secure Stage Overview](https://about.gitlab.com/stages-devops-lifecycle/secure/)
- [Secure Stage Direction and Roadmap](https://about.gitlab.com/direction/secure/)
##### Intermediate Level
- [Static Application Security Testing (SAST) Feature Docs](https://docs.gitlab.com/ee/user/application_security/sast/)
- [Dynamic Application Security Testing (DAST) Feature Docs](https://docs.gitlab.com/ee/user/application_security/dast/)
- [Fuzz Testing Feature Docs](https://docs.gitlab.com/ee/user/application_security/coverage_fuzzing/)
- [Dependency Scanning Feature Docs](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/)
- [License Compliance Feature Docs](https://docs.gitlab.com/ee/user/compliance/license_compliance/index.html)
- [Secret Detection Feature Docs](https://docs.gitlab.com/ee/user/application_security/secret_detection/)
- [Vulnerability Management Feature Docs](https://docs.gitlab.com/ee/user/application_security/security_dashboard/)

### Protect Stage
***
##### Foundations Level
- [Protect Stage Overview](https://about.gitlab.com/stages-devops-lifecycle/protect/)
- [Protect Stage Direction and Roadmap](https://about.gitlab.com/direction/protect/)
##### Intermediate Level
- [Container Scanning Feature Docs](https://docs.gitlab.com/ee/user/application_security/container_scanning/)
- [Security Orchestration Feature Doc](https://docs.gitlab.com/ee/user/application_security/threat_monitoring/#configuring-network-policy-alerts)
- [Container Host Security Feature Docs](https://docs.gitlab.com/ee/user/project/clusters/protect/container_host_security/index.html)
- [Container Network Security Feature Docs](https://docs.gitlab.com/ee/user/project/clusters/protect/container_network_security/index.html)




#### Kubernetes Certification
***
##### Foundations Level
- [Kubernetes for Beginners Udemy Training](https://www.udemy.com/course/learn-kubernetes/)
- [CKA Udemy Training](https://www.udemy.com/course/certified-kubernetes-administrator-with-practice-tests/)
- [CKAD Udemy Training](https://www.udemy.com/course/certified-kubernetes-application-developer/)
##### Intermediate Level
- [Kubernetes Documentation](https://kubernetes.io/docs/home/)
##### Advanced Level
- [CKA Certification](https://training.linuxfoundation.org/certification/certified-kubernetes-administrator-cka/)
- [CKAD Certification](https://training.linuxfoundation.org/certification/certified-kubernetes-application-developer-ckad)
- [CKS Certification](https://training.linuxfoundation.org/certification/certified-kubernetes-security-specialist/)

### Infrastructure-as-Code

#### HashiCorp Terraform
***
##### Foundations Level
- [Hashicorp Learn Tutorials](https://learn.hashicorp.com/terraform)
- [GitLab Terraform Integration Docs](https://docs.gitlab.com/ee/user/infrastructure/)
##### Intermediate Level
- [Terraform Docs](https://www.terraform.io/docs/index.html)
- [Terraform AWS Provider Docs](https://registry.terraform.io/providers/hashicorp/aws/latest/docs)
- [Terraform GCP Provider Docs](https://registry.terraform.io/providers/hashicorp/google/latest/docs)
- [Terraform GitLab Provider Docs](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs)
- [GitLab Sandbox Cloud](/handbook/infrastructure-standards/realms/sandbox/#how-to-get-started)
- [GitLab Demo Systems Terraform Modules](https://gitlab.com/gitlab-com/demo-systems/terraform-modules)
##### Advanced Level
- [Terraform Associate Certification](https://www.hashicorp.com/certification/terraform-associate)

#### HashiCorrp Vault
***
##### Foundations Level
- [Hashicorp Learn Tutorials](https://learn.hashicorp.com/terraform)
##### Intermediate Level
- TODO
##### Advanced Level
- [Vault Associate Certification](https://learn.hashicorp.com/terraform)

#### HashiCorp Consul
***
##### Foundations Level
- [Hashicorp Learn Tutorials](https://learn.hashicorp.com/consule)
##### Intermediate Level
- TODO
##### Advanced Level
- [Consul Associate Certification](https://www.hashicorp.com/certification/consul-associate)



## Additional Resources

- [Field Enablement Handbook Page](/handbook/sales/field-operations/field-enablement/)
- [ Field Certification Handbook Page](/handbook/sales/training/field-certification/)
- [Sales Training](/handbook/sales/training/)
- [O'Reilly](https://learning.oreilly.com/home/). Contact your manager for a license.
- [Communities of Practice](/handbook/customer-success/initiatives/communities-of-practice.html)
- [Learn at GitLab](https://about.gitlab.com/learn/)
