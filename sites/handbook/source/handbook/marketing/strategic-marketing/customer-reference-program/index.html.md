---
layout: handbook-page-toc
title: "Customer Reference Program"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Customer reference program at GitLab

The goal of the Customer Reference Program is to provide opportunities for customers to share their story on how GitLab has helped them overcome the challenges, blockers, and pain points within their organizations. The Reference Program Managers work as a conduit to help connect customers with opportunities by balancing customer requests with company demands. By providing a single point for outreach to customers, the CR team prevents customer burnout, sales team burnout, and ensures a diversity of customer assets are delivered to stakeholders.

## Who we are

Team of Experienced Customer Reference Professionals who are focused on managing Reference relationships, translating the customer experience into insights that support Marketing and Sales initiatives.

# Strategy and Process of Reference Program

Our mission is to manage our customer reference relationships like the precious resources they are in order to achieve prolonged success for both our customers and GitLab.

# Execution and Resources of Reference Program

Delivery of Customer Case Studies, Videos, Quotes, Reviews, etc.

# Quick links grids

[**Account Owners!!!  _NOMINATE_ a new _Reference_ Customer in Salesforce here!**](https://about.gitlab.com/handbook/marketing/strategic-marketing/customer-reference-program/#process-for-nominating-new-reference-customers)

| Quick Reference Links: | Customer Reference Program Assets |
| ----------- | ----------- |
| [Customer Reference Standard Agreement and Logo Approval form (case study, earnings, marketing, etc) (updated 2022-02-09))](https://docs.google.com/document/d/1xybajqXhSPeYoKcQXuPy-HGjcHfza9GsPkFMuSoo8FU/edit?usp=sharing) | [Customer Logo Approval form (logo usage only) (04.21.2021)](https://docs.google.com/document/d/1kDbsect_qjrSElUyU95V1hRnFZ9rFgfbO8aHCeW1EIc/edit) | 
| [Customer Case Study Slides - Sales Asset](https://docs.google.com/presentation/d/1YuP5_7LOnTMDcvl9UKPqim7lxaush4SUXDtTQ0uA4Sg/edit?usp=sharing) | [Customer Peer Reviews - Customer Quotes - Sales Deck Asset](/handbook/marketing/strategic-marketing/customer-reference-program/peer-reviews/) |
| [G2 Promotional Badges - Sales Asset](https://docs.google.com/presentation/d/1_cF8vR54V5jAh0lIH5BwoFEVvEK8sBg4NAy39IhLQlk/edit) | [TEI Sales Metrics Slides - Sales Asset](https://docs.google.com/presentation/d/1qGYIf_5L-gZkROLSVqwo8OVDFYqB-qG8NBQ8YHx2ZQY/edit?usp=sharing) |
| [Offical Letter Welcoming Customers to Reference Program](https://drive.google.com/file/d/1pKy7JMY1As_1jepsF7yc6Ae5fQZRCSYE/view?usp=sharing) | [Forrester TEI 2020 Page](https://about.gitlab.com/handbook/marketing/strategic-marketing/analyst-relations/forrester-tei/) - Analyst Report, Sales Asset  |
| [Online TEI Calculator](https://tools.totaleconomicimpact.com/go/gitlab/devopsplatform/index.html) - Share with Anyone | [Customer Approved Quote Deck](https://docs.google.com/presentation/d/1sJjZu5E_rmovnR0fZpO9jM5Clxi6pjqtrFFvzix8S9c/edit?usp=sharing) - Sales Asset |
| [Reference Program Pitch Deck Slide](https://docs.google.com/presentation/d/12_oodxTM50m-Sn0LB4Wvt8f2h9XvIgXzv6Y6TaO-FNk/edit?usp=sharing) - Sales Asset | [Customer Reference One Page Resource](https://docs.google.com/document/d/1UjRCW3_aGVZ4IbApqnUFkuYkxunr7mMfHwWuvR7aqns/edit#) | 


| Quick Reference Links: | Customer Reference Program Processes and Support |
| ----------- | ----------- |
| [Customer Insight Page (Case Study Process)](https://about.gitlab.com/handbook/marketing/strategic-marketing/customer-reference-program/customer-insight/) | [Requesting a reference for a sales call](/handbook/marketing/strategic-marketing/customer-reference-program/index.html#requesting-a-reference-customer-to-support-a-sales-call) |
| [Customer Case Studies - Customers Page](https://about.gitlab.com/customers/) | [Case Study Board - search by Usecase, Value Driver, Product replacing, etc](https://gitlab.com/gitlab-com/marketing/strategic-marketing/customer-reference-content/case-study-content/-/boards/1804878?scope=all&utf8=%E2%9C%93&state=opened) |
| [Customer Events - Process and Support](https://about.gitlab.com/handbook/marketing/strategic-marketing/customer-reference-program/customer-events.html) | [Customer Logo Approval form (case study etc) (06.21.2021)](https://docs.google.com/document/d/1Ow2_bekQyzMGBRBxEHXo41V4q82gHLSY-N5APtyytJE/edit) |
| [Email Copy for Case Study Reference](https://docs.google.com/document/d/1dSHnDSjuWG77Yb2hc4zbMY_zPNbo2Oycctkzre2w3VQ/edit) | [SFDC Report of Recently Referenced Customers](https://gitlab.my.salesforce.com/00O4M000004dvWq) |
| [Email Template - Analyst Reference](https://docs.google.com/document/d/1ue-1MyBT0FHg6gnu9ZM81X35-lXkQz2p852SRNrzYYo/edit) | [Press Kit - GitLab Brand Guidelines and Logos]https://about.gitlab.com/press/press-kit/) |
|[Full Customer Logo Deck Jan 2022](https://docs.google.com/presentation/d/1UKVfxDm6KNpWCjoAcWp0AfX1yaLT10FK-zbbFZLAwZE/edit#slide=id.gc300321b28_0_273) |
| [Customer Advisory Board](/handbook/marketing/strategic-marketing/customer-reference-program/CAB/) | [Third-Party Authorization to use GitLab Trademarks](https://about.gitlab.com/handbook/legal/marketing-collaboration/#third-party-use-of-gitlab-trademarks-on-marketing-materials) |
| [Customer Reference Registration Page (external)](https://about.gitlab.com/handbook/marketing/strategic-marketing/customer-reference-program/external-nomination/) | [Media Consent and Model Release Form - Updated 2021](https://drive.google.com/file/d/1-Ri8Hubnt3A7QolnwjBPYyM0m2hTxnG0/view?usp=sharing) |






## Goals of the GitLab Customer Reference Program
- **Credibility** - Reinforce our credibility as an end-to-end DevOps solution partner.
- **Shorten the sales cycle** - Having our advocates involved with sharing their story with potential sales, analysts and the marketplace can help potential sales close sooner
- **Increase Revenue** - A successful Customer Reference Program helps increase revenue by attracting new clients and increasing retention rates.
- **Customer Advocacy** - Encourage our customers to share their success stories with others who are learning about or evaluating GitLab.
- **Align with our customer value drivers** - Our program reflects our customer values drivers of increasing operational efficiencies, delivering better products faster and reducing security and compliance risk.
- **Align with our customer use cases** - Our program supports our [customer use cases](/handbook/use-cases/)


## Process for nominating new reference customers

 - **[Enablement Video for Nomination Process](https://www.youtube.com/watch?v=8Le_Ovglnq8&list=PL05JrBw4t0KrirMKe3CyWl4ZBCKna5rJX&index=63)**
 
   1. Sales team member nominates champion by pushing the [**Nominate a Reference** button on the **individual champion's name** in SFDC.](https://gitlab.my.salesforce.com/home/home.jsp) 
   2. Sales team member works with the Technical Account Manager (TAM) to fill out the details for used stages, product purchased, and referenceable activities.
   3. CRM team is notified of nomination in SFDC, CRM creates a GitLab issue to manage the reference process
   4. If additional information is needed, CRM team will outreach to Account owner.
        - Account Owner may need to connect champion with customer reference team


### External Customer Reference Landing Page
We have an [external reference page](https://page.gitlab.com/reference.html) that is integrated into customer communications to promote customers to register their interest in joining our Reference Progam.  The process for managing these responses is outlined [here](https://about.gitlab.com/handbook/marketing/strategic-marketing/customer-reference-program/external-nomination/) 

### ReferenceEdge - using Reference Lead Finder and Reference Prospector
We use workflow functionality within ReferenceEdge to encourage sales account owners to nominate customers into the reference program. Find detailed process [here](https://docs.google.com/presentation/d/1sQdjtScw8jy_M2Y40mtAeCNyzNSk3G7tV4kypmlg8u0/edit#slide=id.geb24fb749c_0_5). The Customer Reference Manager reviews the [reference lead report](https://gitlab.my.salesforce.com/a7M?fcf=00B4M000004oXPE) on a quarterly basis and reaches out to sales to gather customer insight.  

### Customer references collection
Our written customer case studies are found on the [/customers page](https://about.gitlab.com/customers/)
Our video customer case studies are found on the YouTube site on [this playlist](https://www.youtube.com/playlist?list=PLFGfElNsQthZ__Rq59rec-EMgKsLuNjJE)

### Customer Reference Types
Some examples of the types of assets we'd use as customer references once we have approval from the customer:
* Logo (The ability to name a company as a customer and use their logo in our marketing materials)
* Live sales reference (both calls and possible in-person)
* Website content
  * Written case studies (See [Customer Insight Page](https://about.gitlab.com/handbook/marketing/strategic-marketing/customer-reference-program/customer-insight/) for case study process)
  * Blog posts
  * Videos
  * Podcasts
  * Usage quotes
* Event speakers (At industry, third-party and company events)
* References for analysts and press
* Quotes (can be attributable with approval or anonymous)
   

### Sample Interview Questions/topics for Customer joining the Reference Program (See [Customer Insight Page](https://about.gitlab.com/handbook/marketing/strategic-marketing/customer-reference-program/customer-insight/) for case study process)
    * What led you to GitLab, what problems were you trying to solve?
    * Why did you choose GitLab and what other tools were you using or considering?
    * What has been your experience with GitLab?  
    * How did you make the business case for GitLab and what metrics have you seen improve?
    * What have you heard from GitLab users, what was their adoption curve like?
    * What has been the most unexpected success you have experienced? Adoption rates? Improved speed?

### Sourcing Reference Support for your sales opportunities
Please access the resources below to search for suitable references to send to your prospects/customers.
   1. [Case Reference Board](https://gitlab.com/gitlab-com/marketing/strategic-marketing/customer-reference-content/case-study-content/-/boards/1804878?scope=all&utf8=✓&state=opened)
   2. [Metrics Reference Board](https://gitlab.com/gitlab-com/marketing/strategic-marketing/customer-reference-content/case-study-content/-/boards/1794898)
   3. Quote Reference Board - **Coming Soon**
If you have specific reference needs that are not addressed in the resources above; please advise us on our slack channel (Customer_References) and also in the customer facing slack channels (SA/TAM/Customer Success) so we can collaborate to support you.

### Requesting a Reference Customer to support a sales call
   1. Sales team member selects "Find a reference" button from the selected opportunity Note: Opportunity required to be in stage 3 or later.
   2. Filter to find the most appropriate reference for your needs.
   3. Fill out required information and include any customer forms that may be required.
   4. Hit submit and wait for the approval for the call from the champion's account owner
   5. [Sales enablement video on GitLab Unfiltered with this process](https://www.youtube.com/watch?v=8Le_Ovglnq8&list=PL05JrBw4t0KrirMKe3CyWl4ZBCKna5rJX&index=63)
   6. If the sales member is unable to find the correct reference customer from within ReferenceEdge, they should post their needs in the various slack  channels (Customer success/TAM etc) to request support from customer facing teams to identify the correct customer.
   7. Please tag your regional CRM in this sourcing activity so that it can be supported and tracked.

 [Reference Edge Intro Video](https://www.point-of-reference.com/wp-content/uploads/2020/07/WhyWouldntYou-Video.mp4).

### Customer Reference Request Rules of Engagement (Managing Volume & Frequency of Reference Customer Engagement Requests)
It is critical and the Customer Reference Management team’s responsibility to optimize our reference customer engagements to balance:
* Maximizing the positive impact of both the customer’s and GitLab’s investment of time and effort
* Ensuring continuous, long term customer and GitLab success by avoiding exhausting reference customer relationships<br>

By default, we will request a customer support reference call (sales/analyst relations/public relations) no more than once per quarter per customer. In support of event speaking and/or other participation support (field, alliance, digital, community, etc.), we will request customer support at a maximum of once every two quarters per customer. The CRM ultimately decides if the reference request is suitable for the proposed customer based on a number of factors including but not limited to:
* Past, current, and future planned volume
* Frequency
* Effort required of support requests
* Customer's stated comfortable volume and levels of engagement

### Customer Events
To request customer speakers for various activities, please view detailed process [here](https://about.gitlab.com/handbook/marketing/strategic-marketing/customer-reference-program/customer-events.html)

### Creating new issue for Customer Reference Program
To create a general request for the Customer Reference Program, open an issue on the [Customer Reference Program Board](https://gitlab.com/groups/gitlab-com/marketing/-/boards/927283?&label_name[]=Customer%20Reference%20Program) in the *Strategic Marketing Project.*

- Make sure it has the label *Customer Reference Program*. Feel free to add any applicable labels around the request. 
- Assign to Reference Program Manager @jlparker (Jen Parker) for NORAM or @FionaOKeeffe (Fiona O'Keeffe) for EMEA 
- Please give at least 30 days notice, to give proper lead time for sourcing references.  
- Please provide as much clarifying information as you can, include information regarding industry/vertical and campaigns this request may be linked to.
- Slack @jparker or @Fiona O'Keeffe with issue request if your request requires immediate attention, or if you have questions
- For customer speaking requests; please follow this [process](https://about.gitlab.com/handbook/marketing/strategic-marketing/customer-reference-program/customer-events.html) 

### Approved Customer Logos for promotion
GitLab understands the value of our customer relationships and values customers that are willing to share their success with our team. We have a [logo permission form](https://docs.google.com/document/d/1Ow2_bekQyzMGBRBxEHXo41V4q82gHLSY-N5APtyytJE/edit) that we require be signed before we promote the customer relationship using the customer logo. We understand and respect that corporate logos are considered intellectual property and are owned and licensed by the respective organization.

- Approved List of [Customer logos](https://docs.google.com/spreadsheets/d/1wnczZQ7a8rIXI_OCNPuqJv5VeC5gGs-lwAmlgyxUlvU/edit#gid=0). This list is updated from SFDC. If you have any questions on customer logo usage, feel free to reach out to the Customer Reference Team slack Channel: Customer_References.

### Approved Customer Slides
1. Customer Slide in the [Pitch Deck](/handbook/marketing/corporate-marketing/brand-activation/brand-growth/pitch-deck/)
This [slide](https://docs.google.com/presentation/d/1dVPaGc-TnbUQ2IR7TV0w0ujCrCXymKP4vLf6_FDTgVg/edit#slide=id.gcc362bfe78_0_84) is reviewed and updated on a 6 monthly basis by the PMM team and the Reference team. The CRM creates the review issue and works with the assigned PMM to review new logos and create a revised slide. Once the strategic marketing team approves the new slide; the CRM creates an [issue](https://about.gitlab.com/handbook/marketing/inbound-marketing/#brand-and-design-issue-templates) for the brand team to review the slide. The CRM co-ordinates with the Pitch deck editor to update the new slide into the pitch deck.
 
2. Customer Case Study Slides
Once the case study is published, the CRM creates a stand alone slide in the [WIP folder](https://docs.google.com/presentation/d/1me_mH87FiGCnocLPCRnPMp-uFAkZ5NuxHehu5TIG-l0/edit#slide=id.g9006709712_0_0), at the end of every quarter, the CRM raises a design review [issue](https://about.gitlab.com/handbook/marketing/inbound-marketing/#brand-and-design-issue-templates) for the brand team to approve the new case study slides in WIP. When the new slides are approved by the design team; the CRM migrates these new slides into the [Case Study Deck](https://docs.google.com/presentation/d/1YuP5_7LOnTMDcvl9UKPqim7lxaush4SUXDtTQ0uA4Sg/edit#slide=id.g8f271498f2_0_301). 

3. [Full Customer Logo Deck Jan 2022](https://docs.google.com/presentation/d/1UKVfxDm6KNpWCjoAcWp0AfX1yaLT10FK-zbbFZLAwZE/edit#slide=id.gc300321b28_0_273)
The CRM reviews the deck above on a 3 monthly basis; working with the PMM team to update the deck. The CRM creates an [issue](https://about.gitlab.com/handbook/marketing/inbound-marketing/#brand-and-design-issue-templates) for the brand team to review and publishes a new version of the customer logo deck. The CRM manages the versions of the deck and tracks the new additions.  

### Customer Case Study Page Audit
The CRM will review the [customer case study page](https://about.gitlab.com/customers/) on a quartlery basis reviewing the content and the customer grid.

### Authorization to use Company Materials
**To promote our customer reference content, we use the customer logos with respect to reference content created in promotional activities**

Customer logos appear on our website and other marketing and promotional materials. To respect the intellectual property of companies that use our product, we request a signed permission form to use their logo. The approval form includes permission to localise the reference content into different languages hosted on our website, to include the content in analyst relations activities, and to include the content in public relations activities. To request these permissions, please send the [Authorization to Use Company Materials Form](https://docs.google.com/document/d/1Ow2_bekQyzMGBRBxEHXo41V4q82gHLSY-N5APtyytJE/edit) to the appropriate member of the customer organization.
Please note this approval form includes permission for logo usage in case study/video and social/public promotion activities as per prior customer agreement. 
   1. Once we have received back the signed authorization form, send the form to the Customer Reference Team.
   2. The Customer Reference Team will attach the signed form to the account on SFDC.
   3. The Customer Reference Team will add the customer to the Reference Edge software with logo usage selected.
   4. The Customer Reference Team will then add the logo to the customer approved locations (including website and or promotional presentations.)


### GitLab Customer Advisory Boards
Information about the GitLab Customer Advisory Boards [is found on the CAB page](/handbook/marketing/strategic-marketing/customer-reference-program/CAB/)

### Peer Reviews
Learn about our [Peer Review](/handbook/marketing/strategic-marketing/customer-reference-program/peer-reviews/) Management Program

### Custom Program Swag for Customers
The Customer Reference Team has created appreciation swag for customers to thank them for their support and for engaging with us in reference activities. Availability of these items is limited and managed by the Customer Reference Program team.
For customers in the Americas, please reach out to [Jen](mailto:jparker@gitlab.com)
For EMEA-based customers, please reach out to [Fiona](mailto:fokeeffe@gitlab.com)  

## Metrics of the GitLab Customer Reference Program
View information around customer reference pool, keep track of the latest case studies and find out how the Customer Reference team is helping share customer success stories.
- [Metrics Epic](https://gitlab.com/groups/gitlab-com/marketing/-/epics/987)

### Which customer reference team member should I contact?
  - Listed below are areas of responsibility within the Customer Reference team:

    - [Fiona](/company/team/#fokeeffe), Senior Reference Program Manager
    - [Jen](/company/team/#jlparker), Reference Program Manager
    - [Laura](/company/team/#lclymer), Director of Market Strategy and Insights
