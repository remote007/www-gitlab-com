---
layout: handbook-page-toc
title: "Learn How to Use Chorus.ai"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

Confused about how to use Chorus? Check out the instructions and links below for more information on how to get the most out of this important sales tool.

## Logging into Chorus
Log into Chorus by clicking on the Google sign in option on the [Chorus homepage](https://hello.chorus.ai/login?next=http:%2F%2Fchorus.ai%2Fhome) and selecting your GitLab Gmail account.

## Chorus Training & Enablement
- Please familiarize yourself with [GitLab's SAFE Framework](https://about.gitlab.com/handbook/legal/safe-framework/) before utilizing Chorus.
- All ROW team members are required to complete the [Chorus GDPR Training](https://classroom.google.com/u/0/c/MTI5NDQzODc5NzE1) in order to be granted Recorder access. SalesOps will send team members an invitation through Google Classroom to complete the course once their [Baseline Role-Based Entitlement](https://about.gitlab.com/handbook/business-technology/team-member-enablement/onboarding-access-requests/access-requests/baseline-entitlements/) is submitted by their manager.

## Chorus.ai Resources

- [How to Remove Chorus From a Meeting](https://www.loom.com/share/a6513ac235ae4eb9acaaeb167d7583ce)
- [Livestream & Recording Notifications](https://drive.google.com/file/d/135LWz_6u6vgJPVriIoEa3qOYOa4YkM_b/view)
  - How to explain why Chorus is there and why it is recording
- [Security & Compliance](https://www.chorus.ai/trust)
  - Share this with customers as needed
- [Chorus FAQs](https://docs.chorus.ai/hc/en-us/sections/115002365588-FAQs)

## Chorus.ai Basics

- [Getting Started with Chorus](https://docs.chorus.ai/hc/en-us/sections/115002365608-Getting-Started-with-Chorus)
- [Chorus Basics for SDRs and Reps](https://docs.chorus.ai/hc/en-us/sections/360003251593-Chorus-Basics-for-SDRs-BDRs-and-Reps)
- [Chorus Basics for Managers & Sales Enablement](https://docs.chorus.ai/hc/en-us/sections/115002370787-Chorus-Basics-for-Managers-Sales-Enablement)
- [What’s the maximum length of a Chorus recorded meeting?](https://docs.chorus.ai/hc/en-us/articles/360045702734-What-s-the-maximum-length-of-a-Chorus-recorded-meeting-)

## Security & Privacy Controls

- The retention policy for recorded calls in Chorus is 90 days as of March 1, 2022
- To preserve snippets of calls in EdCast indefinitely, approval from Legal must be obtained by pinging the #legal channel in Slack
- To Download recordings & create playlists the User must obtain approval from Legal by pinging the #legal channel in Slack
  - Snippets saved to any playlists will be preserved
  - Snippets saved on the call will be deleted according to the retention policy
- All Team Members approved for Recorder status are armed with a talk track to inform participants why the call is being recorded and letting them know they can request the call to not be recorded. If the Customer objects, the recording must be turned off or a new call must be initiated.
- Any time a Chorus link is shared, a password should be required or the link should be set to expire. Each user has to do this; it can not be turned on globally from the admin panel.
- As a best practice, please DO NOT include links to Chorus recordings in a public issue. If you need to include a Chorus recording or any type of customer information in an issue, please first mark the issue as `Confidential`.
For additional details, please see [this issue](https://gitlab.com/gitlab-com/gl-security/security-operations/sirt/operations/-/issues/1560#note_702890753).
- [Compliance](https://docs.chorus.ai/hc/en-us/sections/360001251353-Compliance)

## Swim Lanes
### Recorder Access
- US Sales
- US Customer Success
- US SDRs
- ROW Sales upon completion of [Chorus GDPR Training](https://classroom.google.com/u/0/c/MTI5NDQzODc5NzE1)
- ROW SDRs upon completion of [Chorus GDPR Training](https://classroom.google.com/u/0/c/MTI5NDQzODc5NzE1)

### Listener Access
- May be requested through an [Access Request](https://about.gitlab.com/handbook/business-technology/team-member-enablement/onboarding-access-requests/access-requests/#how-do-i-choose-which-template-to-use) and granted on a case by case basis

## Why is meeting live on chorus?

- Zoom and Chorus have gone through a lot of legal and development cycles to be transparent in what data is being used for all participants, so the verbiage cannot be customized at this time.

## Recording with Breakout Rooms

- Chorus will not capture the breakout rooms, only the main Zoom meeting will be captured.
- Please note: By default Chorus has rules around silence on a call. If participants are put into breakout rooms for longer than 5 minutes in the middle of the call, Chorus will drop out of the call due to silence thinking the meeting is over.

## Chorus.ai Advanced Usage

- [Chorus Integrations](https://docs.chorus.ai/hc/en-us/sections/115002215568-Integrations)
