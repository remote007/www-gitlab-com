---
layout: handbook-page-toc
title: Learning Initiatives
description: "The Learning & Development team is rolling out learning programs to enable a culture of curiosity and continual learning."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Learning Initiatives Introduction

The Learning & Development team is rolling out learning programs to enable a culture of curiosity and continual learning. The learning initiatives are intended to provide a forum for team members to come together to learn about all of the activities taking place in the learning space, as well as hear from leaders internally and externally on various leadership topics.

Check out our [monthly announcements](https://about.gitlab.com/handbook/people-group/learning-and-development/learning-initiatives/monthly-announcements) for upcoming programs for team members.

We record our [past initiatives](/handbook/people-group/learning-and-development/learning-initiatives/past-initiatives) so that we can share and revisit our programs over time.

## Learning Sessions

### Live Learning

Live Learning sessions will be conducted on a monthly basis. There will be a Zoom video conference set up for each session. Official dates and topics will be added to the schedule below as confirmed. If you were unable to attend a live learning session but still want to learn, check out our past live learning sessions below. At GitLab we [give agency](/handbook/values/#give-agency), but if you are attending Live Learning sessions, you will be asked to be engaged and participate with your full attention.

<details>
  <summary markdown='span'>
    Format for Live Learning Sessions
  </summary>

<b>Format for 25 minute sessions:</b>

<ul>
<li>10 minutes - introduction/content</li> 
<li>10-15 minutes - Q&A</li>
</ul>

<b>Format for 50 minute sessions (times below are approximate):</b>

<ul>
<li>10-15 minutes - introduction/content</li>
<li>10-20 minutes - breakout session</li>
<li>10-20 minutes - debrief</li>
<li>5 minutes - conclusion</li>
</ul>

</details>

<details>
  <summary markdown='span'>
    Live Learning Schedule
  </summary>

<b>The upcoming Live Learning schedule is as follows:</b>

<ul>
<li>January - 3 Week Manager Challenge</li>
</ul>

</details>


<details>
  <summary markdown='span'>
    Past Live Learning Sessions
  </summary>

<b>2020</b>

<ul>
<li>January - <a href="https://youtu.be/crkPeOjkqTQ">Compensation Review: Manager Cycle (Compaas)</a></li>
<li>January - <a href="/company/culture/inclusion/being-an-ally/">Ally Training</a></li>
<li>February - <a href="/handbook/people-group/guidance-on-feedback/#receiving-feedback">Receiving Feedback</a></li>
<li>June - <a href="/handbook/people-group/guidance-on-feedback/#guidelines-for-delivering-feedback">Delivering Feedback</a></li>
<li>June - <a href="/company/culture/inclusion/unconscious-bias/">Recognizing Bias</a></li>
<li>September - <a href="/handbook/people-group/learning-and-development/manager-challenge/#pilot-program">Manager Challenge Pilot</a></li>
<li>November - <a href="https://www.youtube.com/watch?v=WZun1ktIQiw">Belonging</a></li>
<li>November - <a href="/handbook/leadership/emotional-intelligence/psychological-safety/#one-week-challenge">One Week Challenge - Psychological Safety</a></li>
<li>December - <a href="/handbook/leadership/coaching/#introduction-to-coaching-1">Introduction to Coaching</a></li>
</ul>

<b>2019</b>

<ul>
<li>November - <a href="/company/culture/all-remote/effective-communication/">Communicating Effectively & Responsibly Through Text</a></li>
<li>December - <a href="/company/culture/inclusion/being-inclusive/">Inclusion Training</a></li>
</ul>

</details>


### Social Learning Through Live Learning

Social Learning is the cornerstone for how L&D designs, develops, and delivers live learning sessions. Social learning focuses on how team members interact with peers for just-in-time learning and skill acquisition through knowledge retention. Live learnings serve as an opportunity for team members to build relationships and a sense of community with team members. Social Learning occurs when team members come together in a virtual forum synchronously to learn from others through networking, breakout groups, storytelling, lessons learned reflection, and collaboration on solving scenarios with role playing. The live learnings enables learners to pull knowledge fro experts and peers within the organization instead of having knowledge pushed to them. 

Example of a Social Learning Live Learning Session on [Building High Performing Teams](/handbook/leadership/build-high-performing-teams/)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/aezVF1nOBWc" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

Social Learning can also occur in GitLab's Learning Experience Platform - [GitLab Learn](https://gitlab.edcast.com/) and asynchronous forums using GitLab. (i.e. [Manager Challenge](https://gitlab.com/groups/gitlab-com/people-group/learning-development/-/epics/26))


## Learning Speaker Series Overview

The L&D team hosts a monthly Learning Speaker Series call, open to all team members. The calls serve to provide a space where senior executives and mid-level people leaders can share relevant topics to engage and teach team members lessons learned from their own careers and answer relevant L&D related questions. We also hope to use this forum for external speakers to teach topics on relevant skills for team members.

### Goals

1. Provide a forum where team members can hear from established leaders on how they've managed their career and lessons learned to share with the wider community.

### Examples of Call Topics

This list of topics can be used as a starting point for brainstorming content and connecting with individuals to feature during Learning Speaker Series calls. This is not an exhasutive list - please add new ideas to this list as they come up in each monthly call!

- L&D Initiatives
- Leading Teams
- Managing Remote Teams
- Managing Underperformance
- [Crucial Conversations](/handbook/people-group/learning-and-development/learning-initiatives/crucial-conversations)
- Coaching
- Leadership Best Practices
- Developing Emotional Intelligence
- Developing High Performing Teams
- Managing Conflict

### Past Learning Speaker Series calls

Check out recordings of previous Learning Speaker Series calls!

- 2020-11-19 [Building Trust with Remote Teams Learning Speaker Series](https://www.youtube.com/watch?v=hHMDY77upAE&feature=youtu.be)
- 2020-12-10 [Managing Burnout with Time Off](/company/culture/all-remote/mental-health/#rest-and-time-off-are-productive)

### Hosting a Learning Speaker Series call

The following process outlines steps for the L&D team to take when planning and implementing the calls.

Anyone or any team can host a Learning Speaker Series for the organization! If interested and there is a topic & speaker lined up, please fill out a [Learning & Development Request template](https://gitlab.com/gitlab-com/people-group/learning-development/general/-/blob/master/.gitlab/issue_templates/learning-and-development-request.md) to begin the process.

**Planning Issue Template:** Open a `learning-speaker-series` issue template in the L&D [General Project](https://gitlab.com/gitlab-com/people-group/learning-development/general/-/blob/master/.gitlab/issue_templates/learning-speaker-series.md) to host a session. Steps outlined below are also included in the issue template and are included here as reference.

1. Collaborate with exec team, mid-level people leaders, and other interested GitLab Team members to gauge interest and availability to be featured in the call. Be mindful to include diverse voices from across the GitLab team.
1. Determine if this is an internal or external speaker for the series call. If external speaker, ensure that the speaker has been validated and that the topic covered aligns with our values. External speaker presentations need to aling with how we deliver group conversations.
1. Post in the [#people-connect Slack channel](https://app.slack.com/client/T02592416/C0SNC8F2N/thread/C0SNC8F2N-1602618225.269200) to coordinate a Zoom meeting on the company calendar. This meeting should be set up to live stream to the GitLab unfiltered YouTube channel. Consider hosting the call at least 2 times to best accommodate for multiple time zones.
1. Test Zoom functionality with the speaker at least two business days before event.
    - If external speaker, ensure they have downloaded [Zoom](https://zoom.us/support/download).
    - Have speaker test [audio and video](/handbook/tools-and-tips/zoom/#how-to-test-audio-and-video-in-zoom).
    - Check that speaker will be able to [share a presentation](/handbook/tools-and-tips/zoom/#how-to-share-a-presentation-in-zoom) if necessary.
1. Send speaker calendar invite with all details for the call (including, but not limited to, Zoom link, agenda, slides, etc.).
1. Create slide deck for presentation. Make a copy of a previous month's presention in the [Continuous Learning Campaign Google Folder](https://drive.google.com/drive/folders/1d4ksJXBMrATATxN0QyJ4FA6hzchMNdvb?usp=sharing)
1. Open a feedback issue for questions and comments to be shared asynchronously.
1. Coordinate an announcement of the call in the #company-fyi Slack channel from Sid or another executive/manager who will be featured that month. The post should be shared 1 business day before the call. This post should include a link to the slide deck and coresponding issue. See notes below for a template that can be shared.

#### Text for CEO share in #company-fyi channel

`Join the Learning and Development team on [DATE] for the Learning Speaker Series Call. This month's call features [PERSON] and their experience with [BACKGROUND}. You can review the slide deck for the call [HERE], and post questions you might have in the call adenga doc [HERE]. Looking forward to seeing you there!`

## Learning & Development Quarterly Newsletter

The L&D team also hosts and develops a [quarterly newsletter](/handbook/people-group/learning-and-development/newsletter/) for the community.

## Take Time Out To Learn Campaign

[Focus Friday's](/handbook/communication/#focus-fridays) are a great benefit at GitLab. We try to schedule minimal meetings on Fridays to catch up on work and grow our skills professionally. Use Focus Fridays to take time out to learn. Team members can work with their manager through a [career development conversation](/handbook/leadership/1-1/#career-development-discussion-at-the-1-1) to determine what skills that want to grow in the future. Aspirations can be documented in an [individual growth plan](/handbook/people-group/learning-and-development/career-development/#internal-resources). 

From there, identify what will be needed to attain the new skills and consider using the [Growth and Development Benefit](/handbook/total-rewards/benefits/general-and-entity-benefits/#growth-and-development-benefit) or [expensing professional development opportunities such as coaching, worskshops, conferencces, self-service learning, etc.](/handbook/total-rewards/benefits/general-and-entity-benefits/growth-and-development/#types-of-growth-and-development-reimbursements). Another option you can utilize to learn more is [LinkedIn Learning](/handbook/people-group/learning-and-development/linkedin-learning/). GitLab has a number of LinkedIn Learning licenses for team members. Block time off your calendar every week or month to devote to learning new skills. Share what you are learning with team members and in the #learninganddevelopment slack channel. 

In a [discussion with Darren Murph, Head of Remote at GitLab, about his career development](https://gitlab.edcast.com/pathways/career-development-with-darren-murph/cards/7123731), Darren called out the importance of taking time out to learn, reminding the team that career development is "not something that comes around the fringes of work - it is work". The Take Time out to Learn initiative leans into this idea.

Consider documenting the steps you are going to take learn new skills in the individual growth plan. Check in with your manager and ask for accountablity from them to help you stay aligned with goals.



## Internal Learning Campaigns

Learning campaigns at GitLab have an asynchronous focus and are used to raise awareness around a specific topic or set of resources. These campaigns are inspired by the structure of a learning challenge but without the required engagement or tracking. For example, the L&D team is using a learning campaign structure to host a [mental health awareness week](https://gitlab.com/gitlab-com/people-group/learning-development/general/-/issues/156)

Learning campaigns use a combination of GitLab issues and Slack announcements to raise awareness and spark discussion. Following the campaign, the L&D team will aggregate comments and resources that have surfaced and document in the handbook. Learning campaigns can be used to build buzz around a live speaker series or to start conversation about a common question or issue that GitLab team members are facing.

## LifeLabs Learning

[LifeLabs Learning](https://lifelabslearning.com/) will be piloting their [Manager Core 1](https://drive.google.com/file/d/1MJmxjrMSSCq3lWOOks-vMnzdzPucI0jp/view) and [Manager Core 2](https://drive.google.com/file/d/1f0HhqBfGn1lnaYSMHcYYqauwyOkNHS0R/view) program to a group of ten managers. L&D plans on rolling out more management training courses with LifeLabs in 2023. If interested in partcipating reach out in the `#learninganddevelopment` channel and follow along in `#lifelabs-learnin-pilot` 

Below is the schedule for the pilot program: 

| Workshop | Date | Time | 
| ------ | ------ | ------ |
| Coaching Skills | Thursday 2022-01-13 | 8am-10am PST / 11am-1pm EST / 4pm-6pm GMT |
| Feedback Skills | Thursday 2022-01-27 | 8am-10am PST / 11am-1pm EST / 4pm-6pm GMT |
| Productivity & Prioritization | Thursday 2022-02-17 | 8am-10am PST / 11am-1pm EST / 4pm-6pm GMT |
| Effective 1:1s | Wednesday 2022-02-23 | 8am-10am PST / 11am-1pm EST / 4pm-6pm GMT |
| Manager Intensive 1 | Thursday 2022-05-26 | 8am-10am PST / 11am-1pm EST / 4pm-6pm GMT |

## CEO Handbook Learning Sessions

GitLab's Handbook pages grow every day. Each page serves as GitLab's primary source of [learning and development material](/handbook/people-group/learning-and-development/#handbook-first-training-content). Throughout FY22, the L&D team and the CEO will hold recorded interactive learning sessions to analyze Handbook pages. The goal of the sessions will be to incorporate more video-based learning into the handbook.

Three types of CEO handbook learning sessions:

1. **Handbook Readout:** Bit-sized recording where the L&D team and the CEO review what is on the page. (5 minute video)
1. **Handbook Discussion:** Interactive discussion where L&D facilitates an engaging conversation with the CEO and e-group members. We openly discuss the concepts on the page and allow senior leaders to share best practices implementing them. (25 minute video)
1. **Handbook Interview:** The Learning and Development team will create new pages, create sub-pages, or update existing pages based on interview topics discussed during a targeted Q&A. The video will serve as the foundation for new learning content at GitLab.

The video below from Sid and L&D further explains the concepts:
<figure class="video_container"><iframe src="https://www.youtube.com/embed/cB301JaYnsw" height="315" width="560"></iframe></figure>Steps for L&D Team when setting up a CEO Handbook Learning Session:

Preparing for the Call: 

1. Review topic relevant Handbook pages.
1. Determine what topics you want to cover each week
1. Research the respective page that will be discussed or section of a page
1. Complete some work on the page before the session if you can, create an MR that clarifies information or asks Sid for clarity on the topic discussed.
1. Set up a meeting with CEO through EBA. Invite other e-group members if applicable.
1. Create an agenda with talking points and areas to emphasize during video recording with CEO
1. Send Sid the agenda in the #CEO channel at least 24 hours in advance 
1. Hold a handbook learning session and ensure the discussion is fluid and interactive with open and honest dialogue.

During the Call: 

1. Give Sid and other speakers, if applicable, a few minutes at the beginning of the call to populate answers. 
1. Once everyone is ready, stream to YouTube Live. 
1. Make the title of the agenda, the same as the YouTube Video. 
1. Do a quick introduction. 
1. Go through the questions on the agenda. 
1. If questions come up during the interview, add them to the end of the agenda if there is time
1. When there’s nothing on the agenda left, thank him for his time and end the live stream. 

After the Call:  

1. Determine if L&D can create bite-sized videos with the content. Post bite-sized and long-form video on YouTube Unfiltered channel
1. Update Unfiltered page with more information after the Live Stream. See the the description in this [example video](https://www.youtube.com/watch?v=_okcPC9YucA).
1. Edit the live stream thumbnail with a [Title Page](https://docs.google.com/presentation/d/13Pj3bxYK6FFSsiMx2vOAcsB0C6ctl_1lqFH6E5MtgGE/edit#slide=id.gb1c2c2e157_0_115) for each topic. Here is [the template](https://docs.google.com/presentation/d/13Pj3bxYK6FFSsiMx2vOAcsB0C6ctl_1lqFH6E5MtgGE/edit#slide=id.gb1c2c2e157_0_115) for the videos. Write out a new title, screenshot the image, and update thumbnail. 
1. Embed video on the related Handbook page.
1. Edit page with new content, create sub-page, send MR in the CEO channel with a proposal for an interactive handbook discussion.

### List of CEO Handbook Learning Sessions:

1. [Common misperceptions about Iteration](https://www.youtube.com/watch?v=nXfKtnVUhvQ)
1. [No Matrix Organization](https://www.youtube.com/watch?v=E_wegGRv4mA)
1. [Making Decisions](https://www.youtube.com/watch?v=-by6ohMIi_M&feature=emb_title)
1. [Individual Contributor Leadership](https://www.youtube.com/watch?v=d0x-JH3aolM)
1. [Bias Towards Asynchronous Communication](https://www.youtube.com/watch?v=_okcPC9YucA&feature=emb_title)
1. [High Output Management](https://about.gitlab.com/handbook/leadership/high-output-management/#applying-high-output-management)
1. [Giving and Receiving Feedback](https://www.youtube.com/watch?v=vL864Zg2sm4&t=731s)
1. [Managing Underperformance](https://www.youtube.com/watch?v=-mLpytnQtlY&t=637s)
1. [Transitioning from IC to Manager - Engineering](https://www.youtube.com/watch?v=Zeull-tdy6o) 
1. [Manager Mention Merge Requests](https://www.youtube.com/watch?v=e1sTOtveNOk)
1. [Working Groups](/company/team/structure/working-groups/#ceo-handbook-learning-discussion-on-working-groups)
1. [Skip Level Meetings](https://www.youtube.com/watch?v=kAxp0Mam-Rw)
1. [Product Strategy](https://www.youtube.com/watch?v=yI29xFAgKoA)
1. [Mental Wellness Discussion](https://www.youtube.com/watch?v=od_KdZqc69k)

## Skill of the Month 

The L&D team outlines a skill of the month for team members to learn more about a particular topic. This provides team members with direction about what topics to learn more about and where to find information on these topics. This initiative begins in April 2021. 

### Goals

1. Get team members engaged with our learning platforms (GitLab Learn and LinkedIn Learning). 
1. Provide opportunities for Growth and Development. 
1. Encourage team members to take time out to learn. 

### FY22 Topic Outline

This is the list of topics that were covered in FY22. Each skill of the month can be found in the [Skill of the Month (FY22)](https://gitlab.edcast.com/channel/skill-of-the-month-fy22) channel on [GitLab Learn](https://gitlab.edcast.com/). 

- April: Managing Stress
- May: Effective Communication
- June: Coaching
- July: Culture of Feedback
- August: Career Development
- September: Manager of One
- October: Emotional Intelligence
- November: Gratitude and Recognition
- December: Resilience and Reflection
- January: Team Member Wellbeing

### FY23 Topic Outline

This is the list of topics that will be focused on in FY23. Each Skill of the Month can be found in the [Skill of the Month (FY23)](https://gitlab.edcast.com/channel/skill-of-the-month-fy23) channel on [GitLab Learn](https://gitlab.edcast.com/).

- February: Setting and Achieving Goals

### Organizing a Skill of the Month 

The following process outlines steps for the L&D team to take when planning and implementing the Skill of the Month.

Anyone or any team can recommend a topic for the future! If interested, please fill out a [Learning & Development Request template](https://gitlab.com/gitlab-com/people-group/learning-development/general/-/blob/master/.gitlab/issue_templates/learning-and-development-request.md) to begin the process.

**Planning Issue Template:** Open a `skill-of-the-month` issue template in the L&D [General Project](https://gitlab.com/gitlab-com/people-group/learning-development/general/-/tree/master). Steps are outlined in the issue template.

## People Group Self-Care Check in Calls

This optional call occurs weekly on Wednesday and is a space for the People Group to check-in weekly on our own self-care practices. This initiative is currently in a pilot state and we're iterating on the best format. Other teams at GitLab should feel encouraged to adpot a similar call format if they are interested. [Calm](https://www.calm.com/business/blog) offers great blogs and articles about self-care strategies, and a webinar they hosted in March 2021 inspired the start of these weekly social self-care calls.

This call does not have an agenda because the intention is to hold a safe space where peole can speak openly without focusing on documenting discussions.

Call Structure:

- First 10 minutes: Optional share-out of what you've done this week or this day to care for your self. Participants can share verbally or in the Zoom chat
- Second 10 minutes: Discuss one question prompt about self-care, mental wellness, and general wellbeing. The goal is to get participants thinking about how they can integrate self-care strategies into their daily practice

### Example self-care discussion questions

1. What have you managed well in the past year?
1. What brought you joy today? This week? This month?
1. How do you feel at this moment? Why?
1. What advice do you give others? Do you follow it for yourself?
1. When the people I spend time with do ____ or say ____, I feel energized.
1. How do you move past unpleasant thoughts, experiences, or discussions?
1. What do you do that makes you feel most like yourself? Why?


## New team member Slack training

This training is a 10 day, automated message training via Slack to introduce new team members to key strategies for using Slack at GitLab. All content in the training is based on documentation in the [Slack handbook](/handbook/communication/#slack).

[Learn more about the Slack training and sign up here](/handbook/people-group/learning-and-development/learning-initiatives/slack-training).

Please note: Existing team members are welcome to sign up for the new team member Slack training, as long as they understand the content is directed to brand new Slack users.




