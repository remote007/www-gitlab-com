---
layout: handbook-page-toc
title: Business Technology
description: Business Technology
---

<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## <i id="biz-tech-icons" class="far fa-paper-plane"></i>How to reach out to us?

<div class="flex-row" markdown="0">
  <div>
    <h5>Team Member Enablement</h5>
    <a href="/handbook/business-technology/team-member-enablement/" class="btn btn-purple" style="width:170px;margin:5px;">Handbook page</a>
    <a href="https://gitlab.com/gitlab-com/business-technology/team-member-enablement/issue-tracker/-/issues/new" class="btn btn-purple" style="width:170px;margin:5px;">Issue tracker</a>
  </div>
  <div>
    <h5>Enterprise Applications</h5>
    <a href="/handbook/business-technology/enterprise-applications/" class="btn btn-purple" style="width:170px;margin:5px;">Handbook page</a>
    <a href="https://gitlab.com/gitlab-com/business-technology/Business-Technology/-/issues/new" class="btn btn-purple" style="width:170px;margin:5px;">Issue tracker</a>
  </div>
</div>

<div class="flex-row" markdown="0">
  <div>
    <h5>Procurement</h5>
    <a href="/handbook/finance/procurement/" class="btn btn-purple" style="width:170px;margin:5px;">Handbook page</a>
    <a href="/handbook/finance/procurement/#-contacting-procurement" class="btn btn-purple" style="width:170px;margin:5px;">Contact the team</a>
  </div>
  <div>
    <h5>Data Team</h5>
    <a href="/handbook/business-technology/data-team/" class="btn btn-purple" style="width:170px;margin:5px;">Handbook page</a>
    <a href="/handbook/business-technology/data-team/#contact-us" class="btn btn-purple" style="width:170px;margin:5px;">Contact the team</a>
  </div>
</div>

<div class="flex-row" markdown="0">
  <div>
    <h5>IT Security and Compliance</h5>
    <a href="/handbook/business-technology/it-compliance/" class="btn btn-purple" style="width:170px;margin:5px;">Handbook page</a>
    <a href="https://gitlab.com/gitlab-com/business-technology/it-compliance/it-compliance-issue-tracker/-/issues/new" class="btn btn-purple" style="width:170px;margin:5px;">Issue Tracker</a>
  </div>
  <div>
    <h5>Engineering and Infrastructure</h5>
    <a href="/handbook/business-technology/engineering/" class="btn btn-purple" style="width:170px;margin:5px;">Handbook page</a>
    <a href="/handbook/business-technology/engineering/#how-to-engage-us" class="btn btn-purple" style="width:170px;margin:5px;">Contact the team</a>
  </div>
</div>

## <i id="biz-tech-icons" class="far fa-newspaper"></i>What's happening?

- [FY23 OKRs Business Technology](https://gitlab.com/gitlab-com/business-technology/okrs/-/blob/master/README.md)
- FY23 Planning - Information available to internal GitLab team members only
    - [FY23 Planning Slides](https://docs.google.com/presentation/d/1sxkIq9GqXVT8OuXmYRFoIrNNRf0-2FXf15O7l22gOo4/edit#slide=id.g72bedaf9ac_0_0)
    - [FY23 Planning Video](https://www.youtube.com/watch?v=w94bz0bWz3g)

## <i id="biz-tech-icons" class="fas fa-users"></i>The team

### Reaching the Teams

- [How to work with Business Technology](/handbook/business-technology/how-we-work/)
- Groups in GitLab
    - `@gitlab-com/business-technology`
    - `@gitlab-com/business-technology/enterprise-apps`
    - `@gitlab-com/business-technology/team-member-enablement`
    - `@gitlab-com/Finance-Division/procurement-team`
- Channels in Slack
    - [`#business-technology`](https://gitlab.slack.com/archives/C01BLS12V37)
    - [`#bt-team-lounge`](https://gitlab.slack.com/archives/CK4EQH50E)

## Team Building

Strong teams are important in business. Business Technology has incorporated a few [team-building exercises](/handbook/business-technology/team-building/) as an ongoing element of our culture. As a department, we aim to celebrate team spirit, collaboration fun and motivation.
